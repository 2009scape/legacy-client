package org.runite.client;

import org.rs09.client.config.GameConfig;

import com.jogamp.opengl.*;
import java.util.Objects;

final class WaterfallShader implements ShaderInterface {

   static boolean[] customCameraActive = new boolean[5];
   static CacheIndex spritesIndex_probably_2172;
   static int waterfallTextureId = -1;
    static RSString aString_8 = RSString.parse("");
    private int listId;
   private final float[] aFloatArray2174 = new float[4];
   private static RSString aString_2175 = RSString.parse(")4a=");


   static void method1626(byte var0) {
      try {
         QuickChatDefinition.aReferenceCache_3572.clear();
         Class143.aReferenceCache_1874.clear();
         if(var0 <= -124) {
            Class67.aReferenceCache_1013.clear();
         }
      } catch (RuntimeException var2) {
         throw ClientErrorException.clientError(var2, "ob.F(" + var0 + ')');
      }
   }

   static boolean method1627(int var0, byte var1) {
      try {
         WorldListEntry var2 = Class3_Sub8.getWorld(97, var0);
         if(var2 == null) {
            return false;
         } else if (Signlink.anInt1214 != 1 && Signlink.anInt1214 != 2 && Class44.paramModeWhere != 2) {
            RSString var9 = aString_8;
            if (Class44.paramModeWhere != 0) {
               var9 = RSString.stringCombiner(new RSString[]{TextCore.aString_4007, RSString.stringAnimator(var2.worldId - -7000)});
            }

            if (var1 > -2) {
               return false;
            } else {
               RSString var4 = aString_8;
               if (Class163_Sub2.paramSettings != null) {
                  var4 = RSString.stringCombiner(new RSString[]{Class97.aString_1380, Class163_Sub2.paramSettings});
               }
               RSString var5 = RSString.stringCombiner(new RSString[]{RSString.parse("http:)4)4"), var2.address, var9, TextCore.aString_2608, RSString.stringAnimator(Class3_Sub20.paramLanguage), aString_2175, RSString.stringAnimator(Class3_Sub26.paramAffid), var4, TextCore.aString_1133, !Unsorted.paramJavaScriptEnabled ? TextCore.aString_3013 : TextCore.aString_339, TextCore.aString_2610, !Class163_Sub2_Sub1.paramObjectTagEnabled ? TextCore.aString_3013 : TextCore.aString_339, TextCore.aString_1617, Client.paramAdvertisementSuppressed ? TextCore.aString_339 : TextCore.aString_3013});

               try {
                  Objects.requireNonNull(Client.clientInstance.getAppletContext()).showDocument(var5.toURL(), "_self");
               } catch (Exception var7) {
                  return false;
               }

               return true;
            }
         } else {
            GameConfig.IP_ADDRESS = var2.address.toString();
            System.out.println(GameConfig.IP_ADDRESS);
//            GameLaunch.SETTINGS.setIp(var2.address.toString());
            var2.address.method1568();
            Class38_Sub1.accRegistryIp = GameConfig.IP_MANAGEMENT;
            CS2Script.userCurrentWorldID = var2.worldId;
            if (Class44.paramModeWhere != 0) {
               Class162.anInt2036 = 0x9c40 + CS2Script.userCurrentWorldID;
               Class140_Sub6.accRegistryPort = Class162.anInt2036;
               Client.currentPort = CS2Script.userCurrentWorldID + 0xc350;
            }

            return true;
         }
      } catch (RuntimeException var8) {
         throw ClientErrorException.clientError(var8, "ob.E(" + var0 + ',' + var1 + ')');
      }
   }

   public final int method24() {
      try {
         return 0;
      } catch (RuntimeException var2) {
         throw ClientErrorException.clientError(var2, "ob.C()");
      }
   }

   public final void method23(int var1) {
      try {
         GL2 var2 = HDToolKit.gl;
         float var4 = (float)(1 + (var1 >> 3 & 3)) * 0.01F;
         float var3 = -0.01F * (float)(1 + (var1 & 3));
         float var5 = 0 == (var1 & 64)?4.8828125E-4F:9.765625E-4F;
         boolean var6 = (128 & var1) != 0;
         if(var6) {
            this.aFloatArray2174[0] = var5;
            this.aFloatArray2174[1] = 0.0F;
            this.aFloatArray2174[2] = 0.0F;
            this.aFloatArray2174[3] = 0.0F;
         } else {
            this.aFloatArray2174[2] = var5;
            this.aFloatArray2174[1] = 0.0F;
            this.aFloatArray2174[3] = 0.0F;
            this.aFloatArray2174[0] = 0.0F;
         }

         var2.glActiveTexture(GL2.GL_TEXTURE1);
         var2.glMatrixMode(GL2.GL_MODELVIEW);
         var2.glPushMatrix();
         var2.glLoadIdentity();
         var2.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
         var2.glRotatef((float)GroundItem.anInt2938 * 360.0F / 2048.0F, 1.0F, 0.0F, 0.0F);
         var2.glRotatef(360.0F * (float) TextureOperation9.anInt3103 / 2048.0F, 0.0F, 1.0F, 0.0F);
         var2.glTranslatef((float)(-Unsorted.anInt144), (float)(-Unsorted.anInt3695), (float)(-LinkableRSString.anInt2587));
         var2.glTexGenfv(GL2.GL_S, GL2.GL_EYE_PLANE, this.aFloatArray2174, 0);
         this.aFloatArray2174[3] = var3 * (float)HDToolKit.anInt1791;
         this.aFloatArray2174[0] = 0.0F;
         this.aFloatArray2174[2] = 0.0F;
         this.aFloatArray2174[1] = var5;
         var2.glTexGenfv(GL2.GL_T, GL2.GL_EYE_PLANE, this.aFloatArray2174, 0);
         var2.glPopMatrix();
         if(Class88.Texture3DEnabled) {
            this.aFloatArray2174[3] = (float)HDToolKit.anInt1791 * var4;
            this.aFloatArray2174[1] = 0.0F;
            this.aFloatArray2174[0] = 0.0F;
            this.aFloatArray2174[2] = 0.0F;
            var2.glTexGenfv(GL2.GL_R, GL2.GL_OBJECT_PLANE, this.aFloatArray2174, 0);
         } else {
            int var7 = (int)((float)HDToolKit.anInt1791 * var4 * 64.0F);
            var2.glBindTexture(GL2.GL_TEXTURE_2D, Class88.anIntArray1223[var7 % 64]);
         }

         var2.glActiveTexture(GL2.GL_TEXTURE0);
      } catch (RuntimeException var8) {
         throw ClientErrorException.clientError(var8, "ob.B(" + var1 + ')');
      }
   }

   public final void method21() {
      try {
         GL2 var1 = HDToolKit.gl;
         var1.glCallList(1 + this.listId);
      } catch (RuntimeException var2) {
         throw ClientErrorException.clientError(var2, "ob.A()");
      }
   }

   public final void method22() {
      try {
         GL2 var1 = HDToolKit.gl;
         var1.glCallList(this.listId);
      } catch (RuntimeException var2) {
         throw ClientErrorException.clientError(var2, "ob.D()");
      }
   }

   public static void method1630(byte var0) {
      try {
         TextCore.aString_2171 = null;
         spritesIndex_probably_2172 = null;
         customCameraActive = null;
         if(var0 > -112) {
            method1632(-116, 108, 54, -120, 44, 6);
         }
         aString_2175 = null;
      } catch (RuntimeException var2) {
         throw ClientErrorException.clientError(var2, "ob.J(" + var0 + ')');
      }
   }

   private void method1631() {
      try {
         GL2 var2 = HDToolKit.gl;
         this.listId = var2.glGenLists(2);
         var2.glNewList(this.listId, GL2.GL_COMPILE);
         var2.glActiveTexture(GL2.GL_TEXTURE1);
         if(Class88.Texture3DEnabled) {
            var2.glBindTexture(GL2.GL_TEXTURE_3D, waterfallTextureId);
            var2.glTexGeni(GL2.GL_R, GL2.GL_TEXTURE_GEN_MODE, GL2.GL_OBJECT_LINEAR);
            var2.glEnable(GL2.GL_TEXTURE_GEN_R);
            var2.glEnable(GL2.GL_TEXTURE_3D);
         } else {
            var2.glEnable(GL2.GL_TEXTURE_2D);
         }

         var2.glTexGeni(GL2.GL_S, GL2.GL_TEXTURE_GEN_MODE, GL2.GL_EYE_LINEAR);
         var2.glTexGeni(GL2.GL_T, GL2.GL_TEXTURE_GEN_MODE, GL2.GL_EYE_LINEAR);
         var2.glEnable(GL2.GL_TEXTURE_GEN_S);
         var2.glEnable(GL2.GL_TEXTURE_GEN_T);
         var2.glActiveTexture(GL2.GL_TEXTURE0);
         var2.glEndList();
         var2.glNewList(this.listId + 1, GL2.GL_COMPILE);
         var2.glActiveTexture(GL2.GL_TEXTURE1);
         if(Class88.Texture3DEnabled) {
            var2.glDisable(GL2.GL_TEXTURE_3D);
            var2.glDisable(GL2.GL_TEXTURE_GEN_R);
         } else {
            var2.glDisable(GL2.GL_TEXTURE_2D);
         }

         var2.glDisable(GL2.GL_TEXTURE_GEN_S);
         var2.glDisable(GL2.GL_TEXTURE_GEN_T);
         var2.glActiveTexture(GL2.GL_TEXTURE0);
         var2.glEndList();
      } catch (RuntimeException var3) {
         throw ClientErrorException.clientError(var3, "ob.I(" + 2 + ')');
      }
   }

   public WaterfallShader() {
      try {
         this.method1631();
      } catch (RuntimeException var2) {
         throw ClientErrorException.clientError(var2, "ob.<init>()");
      }
   }

   static void method1632(int var0, int var1, int var2, int var3, int var4, int var5) {
      try {
         if(var0 <= 66) {
            method1630((byte)-33);
         }

         for(int var6 = var3; var6 <= var1; ++var6) {
            TextureOperation18.method282(Class38.anIntArrayArray663[var6], var4, 121, var2, var5);
         }

      } catch (RuntimeException var7) {
         throw ClientErrorException.clientError(var7, "ob.G(" + var0 + ',' + var1 + ',' + var2 + ',' + var3 + ',' + var4 + ',' + var5 + ')');
      }
   }

}
